<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">face</i>
                    </div>
                    <div class="card-content">

                        <ul class="nav nav-pills nav-pills-warning">
                            <li class="active">
                                <a href="#admins" data-toggle="tab" aria-expanded="true">Admins</a>
                            </li>
                            <li class="">
                                <a href="#app_users" data-toggle="tab" aria-expanded="false">Site Users</a>
                            </li>
                           

                        </ul>

                        <div class="tab-content">
                            <div class="material-datatables tab-pane active" id="admins">
                                <h4 class="card-title">Admins</h4>
                                <div class="toolbar">
                                    <a href="<?php echo base_url('cms/' . $ControllerName . '/add'); ?>">
                                        <button type="button"
                                                class="btn btn-primary waves-effect w-md waves-light m-b-5">Add Admin
                                            User
                                        </button>
                                    </a>
                                </div>
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>

                                        <th>User Name</th>
                                        
                                        <th>Email</th>
                                        <th>Mobile No.</th>
                                        <th><?php echo lang('is_active'); ?></th>

                                        <?php if (checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanEdit') || checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanDelete') || checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanView')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($admins) {
                                        foreach ($admins as $value) { ?>
                                            <tr id="<?php echo $value->UserID; ?>">

                                                <td><?php echo $value->FullName; ?></td>
                                                
                                                <td><?php echo $value->Email; ?></td>
                                                <td><?php echo $value->Mobile; ?></td>


                                                <td><?php echo($value->IsActive ? lang('yes') : lang('no')); ?></td>

                                                <?php if (checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanEdit') || checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanDelete') || checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanView')) { ?>
                                                    <td>
                                                        <!-- <a href="<?php echo base_url('cms/' . $ControllerName . '/changePassword/' . base64_encode($value->UserID)); ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Change Password">lock</i><div class="ripple-container"></div></a>-->
                                                        <?php if (checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanView')) { ?>
                                                            <!--<a href="<?php echo base_url('cms/' . $ControllerName . '/view/' . base64_encode($value->UserID)); ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="View">visibility</i><div class="ripple-container"></div></a>-->
                                                        <?php } ?>
                                                        <?php if (checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanEdit')) { ?>
                                                            <a href="<?php echo base_url('cms/' . $ControllerName . '/edit/' . $value->UserID); ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                                        class="material-icons" title="Edit">edit</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                        <?php if (checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanDelete')) { ?>
                                                            <a href="javascript:void(0);"
                                                               onclick="deleteRecord('<?php echo $value->UserID; ?>','cms/<?php echo $ControllerName; ?>/action','')"
                                                               class="btn btn-simple btn-danger btn-icon remove"><i
                                                                        class="material-icons" title="Delete">close</i>
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        <?php } ?>
                                                        
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="material-datatables tab-pane" id="app_users">
                                <h4 class="card-title">Site Users</h4>
                                <div class="toolbar">
                                    <!--<a href="<?php /*echo base_url('cms/' . $ControllerName . '/add'); */?>">
                                        <button type="button"
                                                class="btn btn-primary waves-effect w-md waves-light m-b-5">Add Admin
                                            User
                                        </button>
                                    </a>-->
                                </div>
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>

                                        <th>Full name</th>
                                        
                                        
                                        <th>Email</th>
                                        <th>Mobile No.</th>
                                        <th><?php echo lang('is_active'); ?></th>
                                        

                                        <?php if (checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanEdit') || checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanDelete') || checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanView')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($app_users) {
                                        foreach ($app_users as $value) { ?>
                                            <tr id="<?php echo $value->UserID; ?>">
                                                <td><?php echo $value->FullName !== '' ? $value->FullName : 'N/A'; ?></td>
                                               
                                               
                                               
                                                <td><?php echo $value->Email; ?></td>
                                                <td><?php echo $value->Mobile; ?></td>

                                                <td><?php echo($value->IsActive ? lang('yes') : lang('no')); ?></td>
                                                 

                                                <?php if (checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanEdit') || checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanDelete') || checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanView')) { ?>
                                                    <td>
                                                        <!-- <a href="<?php echo base_url('cms/' . $ControllerName . '/changePassword/' . base64_encode($value->UserID)); ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Change Password">lock</i><div class="ripple-container"></div></a>-->
                                                        

                                                         <?php if (checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanEdit')) { ?>
                                                            <a href="javascript:void(0);" onclick="LogoutUser(<?php echo $value->UserID; ?>);" class="btn btn-simple btn-warning btn-icon edit">Logout From All Devices</a>
                                                        <?php } ?>
                                                        

                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                           
                        </div><!-- tab-content -->
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>

<script type="text/javascript">
    function LogoutUser(user_id) {
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: function () {
                    $.blockUI({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: base_url + '' + 'cms/user/logout_from_all_devices',
                        data: {
                            'UserID': user_id
                        },
                        dataType: "json",
                        cache: false,
                        success: function (result) {
                            showSuccess(result.success);
                            
                        },
                        complete: function () {
                            $.unblockUI();
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    }
    $(document).ready(function () {
         $('.maroof_verify').on('click',function(){
            var user_id = $(this).attr('data-user-id');
            $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: function () {
                    $.blockUI({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: base_url + '' + 'cms/user/updateMaroofVerify',
                        data: {
                            'UserID': user_id,
                            'MaroofVerifyRequest': 0,
                            'MaroofVerified': 1,
                        },
                        dataType: "json",
                        cache: false,
                        success: function (result) {
                            showSuccess(result.success);
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        },
                        complete: function () {
                            $.unblockUI();
                        }
                    });
                },
                cancel: function () {

                }
            }
        });

         });
    });
</script>