<?php
$user = convertEmptyToNA($user);
if ($user->IsActive == 0) {
    $is_active_msg = 'Activate';
    $is_active_cls = "btn btn-success btn-sm";
    $is_active_val = 1;
} elseif ($user->IsActive == 1) {
    $is_active_msg = 'Deactivate';
    $is_active_cls = "btn btn-danger btn-sm";
    $is_active_val = 0;
}
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            User Details
                            <div class="pull-right">
                                <button class="<?php echo $is_active_cls; ?>"
                                        onclick="UserActive('<?php echo $user->UserID; ?>', '<?php echo $is_active_val; ?>');">
                                    <?php echo $is_active_msg; ?>
                                    <div class="ripple-container"></div>
                                </button>
                            </div>
                            <div class="pull-right">
                                <button class="btn btn-danger btn-sm"
                                        onclick="LogoutUser('<?php echo $user->UserID; ?>');">
                                    Logout From All Devices
                                    <div class="ripple-container"></div>
                                </button>
                            </div>
                            <?php if($user->IsEmailVerified == 0){ ?>
                            <div class="pull-right">
                                <button class="btn btn-success btn-sm"
                                        onclick="VerifyUser('<?php echo $user->UserID; ?>', 'email');">
                                    Verify Email
                                    <div class="ripple-container"></div>
                                </button>
                            </div>
                        <?php } ?>
                         <?php if($user->IsMobileVerified == 0){ ?>
                            <div class="pull-right">
                                <button class="btn btn-success btn-sm"
                                        onclick="VerifyUser('<?php echo $user->UserID; ?>', 'mobile');">
                                    Verify Mobile
                                    <div class="ripple-container"></div>
                                </button>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Full Name</label>
                                        <h5><?php echo $user->FullName; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Username</label>
                                        <h5>@<?php echo $user->UserName; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Gender</label>
                                        <h5><?php echo $user->Gender; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Email</label>
                                        <h5><?php echo $user->Email; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Mobile No.</label>
                                        <h5><?php echo $user->Mobile; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">City</label>
                                        <h5><?php echo $user->CityTitle; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Is Email Verified?</label>
                                        <h5><?php echo $user->IsEmailVerified == 1 ? 'Yes' : 'No'; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Is Mobile Verified?</label>
                                        <h5><?php echo $user->IsMobileVerified == 1 ? 'Yes' : 'No'; ?></h5>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Device Type</label>
                                        <h5><?php echo $user->DeviceType; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Device OS</label>
                                        <h5><?php echo $user->OS; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Is Active?</label>
                                        <h5><?php echo $user->IsActive == 1 ? 'Yes' : 'No'; ?></h5>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            Booth Details
                            <div class="pull-right">
                                <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#exampleModal">
                                    Edit Package Expiry Date
                                    <div class="ripple-container"></div>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Booth Name</label>
                                        <h5><?php echo $user->BoothName; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Booth Username</label>
                                        <h5>@<?php echo $user->BoothUserName; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Booth Type</label>
                                        <h5><?php echo $user->BoothType; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Verification</label>
                                        <h5><?php echo $user->Verification; ?></h5>
                                    </div>
                                </div>
                            </div>
                             <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Package Expiry</label>
                                        <h5><?php echo date('d-m-Y',strtotime($user->PackageExpiry)); ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Is Maroof Verifed</label>
                                        <h5><?php echo $user->MaroofVerified == 1 ? 'Yes' : 'No'; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <?php if($user->MaroofVerified){ ?>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Maroof Link</label>
                                       <h5><?php echo $user->MaroofLink; ?></h5>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Payment Account Number</label>
                                        <h5><?php echo $user->PaymentAccountNumber != '' ? mask_number($user->PaymentAccountNumber) : 'N/A'; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Payment Account Holder Name</label>
                                        <h5><?php echo $user->PaymentAccountHolderName; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Payment Account Bank Branch</label>
                                        <h5><?php echo $user->PaymentAccountBankBranch; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Contact Days</label>
                                        <h5><?php echo $user->ContactDays; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Contact Timings</label>
                                        <h5><?php echo($user->ContactTimeFrom != 'N/A' ? getFormattedDateTime($user->ContactTimeFrom, 'h:i A') . ' - ' . getFormattedDateTime($user->ContactTimeTo, 'h:i A') : 'N/A'); ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">About Booth</label>
                                        <h5><?php echo $user->About; ?></h5>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Categories Selected As User</h5>
                    </div>
                    <div class="card-content">
                        <ul>
                        <?php
                        if (!empty($user->UserSelectedCategories)) {
                            foreach ($user->UserSelectedCategories as $category) { ?>
                                <li><?php echo $category->Title; ?></li>
                            <?php }
                        } else { ?>
                            <li>No Categories Selected As User</li>
                        <?php }
                        ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Categories Selected As Booth</h5>
                    </div>
                    <div class="card-content">
                        <ul>
                            <?php
                            if (!empty($user->BoothSelectedCategories)) {
                                foreach ($user->BoothSelectedCategories as $category) { ?>
                                    <li><?php echo $category->Title; ?></li>
                                <?php }
                            } else { ?>
                                <li>No Categories Selected As Booth</li>
                            <?php }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Following As User</h5>
                    </div>
                    <div class="card-content">
                        <ul>
                            <?php
                            if ($user->UserFollowings && count($user->UserFollowings) > 0) {
                                foreach ($user->UserFollowings as $following) { ?>
                                    <li>
                                        <a href="<?php echo base_url('cms/user/view/' . base64_encode($following['UserID'])); ?>"><?php echo $following['FullName']; ?></a>
                                    </li>
                                <?php }
                            } else { ?>
                                <li>No Following As User</li>
                            <?php }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Following As Booth</h5>
                    </div>
                    <div class="card-content">
                        <ul>
                            <?php
                            if ($user->BoothFollowings && count($user->BoothFollowings) > 0) {
                                foreach ($user->BoothFollowings as $following) { ?>
                                    <li><a href="<?php echo base_url('cms/user/view/' . base64_encode($following['UserID'])); ?>"><?php echo $following['FullName']; ?></a></li>
                                <?php }
                            } else { ?>
                                <li>No Following As Booth</li>
                            <?php }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Followers As User</h5>
                    </div>
                    <div class="card-content">
                        <ul>
                            <?php
                            if ($user->UserFollowers && count($user->UserFollowers) > 0) {
                                foreach ($user->UserFollowers as $following) { ?>
                                    <li>
                                        <a href="<?php echo base_url('cms/user/view/' . base64_encode($following['UserID'])); ?>"><?php echo $following['FullName']; ?></a>
                                    </li>
                                <?php }
                            } else { ?>
                                <li>No Followers As User</li>
                            <?php }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Followers As Booth</h5>
                    </div>
                    <div class="card-content">
                        <ul>
                            <?php
                            if ($user->BoothFollowers && count($user->BoothFollowers) > 0) {
                                foreach ($user->BoothFollowers as $following) { ?>
                                    <li><a href="<?php echo base_url('cms/user/view/' . base64_encode($following['UserID'])); ?>"><?php echo $following['FullName']; ?></a></li>
                                <?php }
                            } else { ?>
                                <li>No Followers As Booth</li>
                            <?php }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Orders Received As Booth</h5>
                    </div>
                    <div class="card-content">
                        <div class="material-datatables">
                            <table class="table table-striped table-hover datatables" cellspacing="0" width="100%"
                                   style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Customer Name</th>
                                    <th>Order Tracking #</th>
                                    <th>Order Status</th>
                                    <th>Order Received At</th>
                                    <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                        <th><?php echo lang('actions'); ?></th>
                                    <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($user->OrdersAsBooth) {
                                    $i = 1;
                                    foreach ($user->OrdersAsBooth as $value) { ?>
                                        <tr id="<?php echo $value['OrderID']; ?>">
                                            <td><?php echo $i; ?></td>
                                            <td>
                                                <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($value['BoothID']); ?>"
                                                   target="_blank"><?php echo $value['BoothName']; ?></a></td>
                                            <td><?php echo $value['OrderTrackID']; ?></td>
                                            <?php
                                            $status = $value['OrderStatusID'];
                                            $class = "btn btn-sm";
                                            if ($status == 1) {
                                                $class = "btn btn-sm";
                                            } else if ($status == 2) {
                                                $class = "btn btn-warning btn-sm";
                                            } else if ($status == 3) {
                                                $class = "btn btn-info btn-sm";
                                            } else if ($status == 4) {
                                                $class = "btn btn-success btn-sm";
                                            } else if ($status == 5 || $status == 6) {
                                                $class = "btn btn-danger btn-sm";
                                            }
                                            ?>
                                            <td>
                                                <button class="<?php echo $class; ?>"><?php echo $value['OrderStatus']; ?>
                                                    <div class="ripple-container"></div>
                                                </button>
                                            </td>
                                            <td><?php echo $value['OrderReceivedAt'] != '' ? getFormattedDateTime($value['OrderReceivedAt'], 'd-m-Y h:i:s A') : 'N/A'; ?></td>
                                            <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                <td>
                                                    <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                        <a href="<?php echo base_url('cms/order/view/' . $value['OrderID']); ?>"
                                                           class="btn btn-simple btn-warning btn-icon edit"><i
                                                                    class="material-icons"
                                                                    title="Click to view details">visibility</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                    <?php } ?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                        $i++;
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Orders Placed As User</h5>
                    </div>
                    <div class="card-content">
                        <div class="material-datatables">
                            <table class="table table-striped table-hover datatables" cellspacing="0" width="100%"
                                   style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Booth Name</th>
                                    <th>Order Tracking #</th>
                                    <th>Order Status</th>
                                    <th>Order Received At</th>
                                    <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                        <th><?php echo lang('actions'); ?></th>
                                    <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($user->OrdersAsUser) {
                                    $i = 1;
                                    foreach ($user->OrdersAsUser as $value) { ?>
                                        <tr id="<?php echo $value['OrderID']; ?>">
                                            <td><?php echo $i; ?></td>
                                            <td>
                                                <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($value['BoothID']); ?>"
                                                   target="_blank"><?php echo $value['BoothName']; ?></a></td>
                                            <td><?php echo $value['OrderTrackID']; ?></td>
                                            <?php
                                            $status = $value['OrderStatusID'];
                                            $class = '';
                                            if ($status == 1) {
                                                $class = "btn btn-sm";
                                            } else if ($status == 2) {
                                                $class = "btn btn-warning btn-sm";
                                            } else if ($status == 3) {
                                                $class = "btn btn-info btn-sm";
                                            } else if ($status == 4) {
                                                $class = "btn btn-success btn-sm";
                                            } else if ($status == 5 || $status == 6) {
                                                $class = "btn btn-danger btn-sm";
                                            }
                                            ?>
                                            <td>
                                                <button class="<?php echo $class; ?>"><?php echo $value['OrderStatus']; ?>
                                                    <div class="ripple-container"></div>
                                                </button>
                                            </td>
                                            <td><?php echo $value['OrderReceivedAt'] != '' ? getFormattedDateTime($value['OrderReceivedAt'], 'd-m-Y h:i:s A') : 'N/A'; ?></td>
                                            <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                <td>
                                                    <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                        <a href="<?php echo base_url('cms/order/view/' . $value['OrderID']); ?>"
                                                           class="btn btn-simple btn-warning btn-icon edit"><i
                                                                    class="material-icons"
                                                                    title="Click to view details">visibility</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                    <?php } ?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                        $i++;
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Products Added</h5>
                    </div>
                    <div class="card-content">
                        <div class="material-datatables">
                            <table class="table table-striped table-hover datatables" cellspacing="0" width="100%"
                                   style="width:100%">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Price</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($user->AddedProducts)) {
                                    foreach ($user->AddedProducts as $addedProduct) { ?>
                                        <tr>
                                            <td>
                                                <a href="<?php echo base_url('cms/product/view/' . $addedProduct->ProductID); ?>"><?php echo $addedProduct->Title; ?></a>
                                            </td>
                                            <td><?php echo number_format($addedProduct->ProductPrice, 2); ?><?php echo $addedProduct->Currency; ?></td>
                                            <td>
                                                <a href="<?php echo base_url('cms/product/view/' . $addedProduct->ProductID); ?>"
                                                   class="btn btn-simple btn-warning btn-icon edit" target="_blank"><i
                                                            class="material-icons"
                                                            title="Click to view details">visibility</i>
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Questions Asked</h5>
                    </div>
                    <div class="card-content">
                        <div class="material-datatables">
                            <table class="table table-striped table-hover datatables" cellspacing="0" width="100%"
                                   style="width:100%">
                                <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Question Description</th>
                                    <th>Question Asked At</th>
                                    <?php if (checkUserRightAccess(58, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(58, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                        <th><?php echo lang('actions'); ?></th>
                                    <?php } ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($user->UserQuestions) {
                                    foreach ($user->UserQuestions as $value) {
                                        $value = (object)$value;
                                        ?>
                                        <tr id="<?php echo $value->QuestionID; ?>">
                                            <td><?php echo $value->CategoryName; ?>
                                                / <?php echo $value->SubCategoryName; ?></td>
                                            <td><?php echo text_shorter($value->QuestionDescription); ?></td>
                                            <td><?php echo getFormattedDateTime($value->QuestionAskedAt, 'd-m-Y h:i:s A'); ?></td>
                                            <?php if (checkUserRightAccess(58, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(58, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                <td>
                                                    <?php if (checkUserRightAccess(58, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                        <a href="<?php echo base_url('cms/questions/view/' . $value->QuestionID); ?>"
                                                           class="btn btn-simple btn-warning btn-icon edit"><i
                                                                    class="material-icons"
                                                                    title="Click to view details">visibility</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                    <?php } ?>

                                                    <?php if (checkUserRightAccess(58, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                        <a href="javascript:void(0);"
                                                           onclick="deleteRecord('<?php echo $value->QuestionID; ?>','cms/questions/delete','')"
                                                           class="btn btn-simple btn-danger btn-icon remove"><i
                                                                    class="material-icons" title="Delete">close</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                    <?php } ?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form action="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>/updateExpiry" method="post"
                              onsubmit="return false;" class="form_data" enctype="multipart/form-data" autocomplete="off"
                              data-parsley-validate novalidate>
                            <input type="hidden" name="UserID" value="<?php echo $user->UserID; ?>">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Package Expiry Date</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                    <div class="form-group label-floating">
                            
                            <input type="date" name="PackageExpiry" parsley-trigger="change" required
                                               class="form-control" id="PackageExpiry" value="<?php echo $user->PackageExpiry; ?>">
                    </div>
             </div>
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>

  </form>
    </div>
  </div>
</div>
<script>
    function UserActive(user_id, val) {
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: function () {
                    $.blockUI({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: base_url + '' + 'cms/user/updateIsActive',
                        data: {
                            'UserID': user_id,
                            'IsActive': val
                        },
                        dataType: "json",
                        cache: false,
                        success: function (result) {
                            showSuccess(result.success);
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        },
                        complete: function () {
                            $.unblockUI();
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    }


    function VerifyUser(user_id, verification_of) {
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: function () {
                    $.blockUI({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: base_url + '' + 'cms/user/verifyUser',
                        data: {
                            'UserID': user_id,
                            'Verification': verification_of
                        },
                        dataType: "json",
                        cache: false,
                        success: function (result) {
                            showSuccess(result.success);
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        },
                        complete: function () {
                            $.unblockUI();
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    }


    function LogoutUser(user_id) {
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: function () {
                    $.blockUI({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: base_url + '' + 'cms/user/logout_from_all_devices',
                        data: {
                            'UserID': user_id
                        },
                        dataType: "json",
                        cache: false,
                        success: function (result) {
                            showSuccess(result.success);
                            
                        },
                        complete: function () {
                            $.unblockUI();
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    }

    $(document).ready(function () {
        $('.fancybox').fancybox({
            beforeShow: function () {
                this.title = $(this.element).data("caption");
            }
        });
    }); // ready
</script>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>