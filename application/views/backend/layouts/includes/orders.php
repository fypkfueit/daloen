<?php $orders = getOrders(); ?>
<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-icon" data-background-color="purple">
            <i class="material-icons">face</i>
        </div>
        <div class="card-content">
            <h4 class="card-title">Orders <small>(Only latest 10 records will be shown here)</small></h4>
            <ul class="nav nav-pills nav-pills-warning">
                <li class="active">
                    <a href="#pending_approval" data-toggle="tab" aria-expanded="true">Pending Approval</a>
                </li>
                <li class="">
                    <a href="#pending_payment" data-toggle="tab" aria-expanded="false">Pending Payment</a>
                </li>
                <li class="">
                    <a href="#payment_done" data-toggle="tab" aria-expanded="false">Payment Done</a>
                </li>
                <li class="">
                    <a href="#dispatched_orders" data-toggle="tab" aria-expanded="false">Dispatched Orders</a>
                </li>
                <li class="">
                    <a href="#completed_orders" data-toggle="tab" aria-expanded="false">Completed Orders</a>
                </li>
                <li class="">
                    <a href="#seller_cancelled" data-toggle="tab" aria-expanded="false">Cancelled By Seller</a>
                </li>
                <li class="">
                    <a href="#buyer_cancelled" data-toggle="tab" aria-expanded="false">Cancelled By Buyer</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="material-datatables tab-pane active" id="pending_approval">
                    <table id="" class="datatable table table-striped table-no-bordered table-hover"
                           cellspacing="0" width="100%" style="width:100%">
                        <thead>
                        <tr>
                            <th>Customer Name</th>
                            <th>Booth Name</th>
                            <th>Order Tracking #</th>
                            <th>Order Received At</th>
                            <th><?php echo lang('actions'); ?></th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($orders['pending_approval_orders']) {
                            foreach ($orders['pending_approval_orders'] as $value) { ?>
                                <tr id="<?php echo $value['OrderID']; ?>">
                                    <td>
                                        <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($value['UserID']); ?>"
                                           target="_blank"><?php echo $value['FullName']; ?></a></td>
                                    <td>
                                        <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($value['BoothID']); ?>"
                                           target="_blank"><?php echo $value['BoothName']; ?></a></td>
                                    <td><?php echo $value['OrderTrackID']; ?></td>
                                    <td><?php echo $value['OrderReceivedAt'] != '' ? getFormattedDateTime($value['OrderReceivedAt'], 'd-m-Y h:i:s A') : 'N/A'; ?></td>
                                    <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                        <td>
                                            <a href="<?php echo base_url('cms/order/view/' . $value['OrderID']); ?>"
                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                        class="material-icons"
                                                        title="Click to view details">visibility</i>
                                                <div class="ripple-container"></div>
                                            </a>
                                        </td>
                                    <?php } ?>
                                </tr>
                                <?php
                            }
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
                <div class="material-datatables tab-pane" id="pending_payment">
                    <table id="" class="datatable table table-striped table-no-bordered table-hover"
                           cellspacing="0" width="100%" style="width:100%">
                        <thead>
                        <tr>
                            <th>Customer Name</th>
                            <th>Booth Name</th>
                            <th>Order Tracking #</th>
                            <th>Order Received At</th>
                            <th><?php echo lang('actions'); ?></th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($orders['pending_payment_orders']) {
                            foreach ($orders['pending_payment_orders'] as $value) { ?>
                                <tr id="<?php echo $value['OrderID']; ?>">
                                    <td>
                                        <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($value['UserID']); ?>"
                                           target="_blank"><?php echo $value['FullName']; ?></a></td>
                                    <td>
                                        <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($value['BoothID']); ?>"
                                           target="_blank"><?php echo $value['BoothName']; ?></a></td>
                                    <td><?php echo $value['OrderTrackID']; ?></td>
                                    <td><?php echo $value['OrderReceivedAt'] != '' ? getFormattedDateTime($value['OrderReceivedAt'], 'd-m-Y h:i:s A') : 'N/A'; ?></td>
                                    <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                        <td>
                                            <a href="<?php echo base_url('cms/order/view/' . $value['OrderID']); ?>"
                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                        class="material-icons"
                                                        title="Click to view details">visibility</i>
                                                <div class="ripple-container"></div>
                                            </a>
                                        </td>
                                    <?php } ?>
                                </tr>
                                <?php
                            }
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
                <div class="material-datatables tab-pane" id="payment_done">
                    <table id="" class="datatable table table-striped table-no-bordered table-hover"
                           cellspacing="0" width="100%" style="width:100%">
                        <thead>
                        <tr>
                            <th>Customer Name</th>
                            <th>Booth Name</th>
                            <th>Order Tracking #</th>
                            <th>Order Received At</th>
                            <th><?php echo lang('actions'); ?></th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($orders['payment_done_orders']) {
                            foreach ($orders['payment_done_orders'] as $value) { ?>
                                <tr id="<?php echo $value['OrderID']; ?>">
                                    <td>
                                        <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($value['UserID']); ?>"
                                           target="_blank"><?php echo $value['FullName']; ?></a></td>
                                    <td>
                                        <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($value['BoothID']); ?>"
                                           target="_blank"><?php echo $value['BoothName']; ?></a></td>
                                    <td><?php echo $value['OrderTrackID']; ?></td>
                                    <td><?php echo $value['OrderReceivedAt'] != '' ? getFormattedDateTime($value['OrderReceivedAt'], 'd-m-Y h:i:s A') : 'N/A'; ?></td>
                                    <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                        <td>
                                            <a href="<?php echo base_url('cms/order/view/' . $value['OrderID']); ?>"
                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                        class="material-icons"
                                                        title="Click to view details">visibility</i>
                                                <div class="ripple-container"></div>
                                            </a>
                                        </td>
                                    <?php } ?>
                                </tr>
                                <?php
                            }
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
                <div class="material-datatables tab-pane" id="dispatched_orders">
                    <table id="" class="datatable table table-striped table-no-bordered table-hover"
                           cellspacing="0" width="100%" style="width:100%">
                        <thead>
                        <tr>
                            <th>Customer Name</th>
                            <th>Booth Name</th>
                            <th>Order Tracking #</th>
                            <th>Order Received At</th>
                            <th><?php echo lang('actions'); ?></th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($orders['dispatched_orders']) {
                            foreach ($orders['dispatched_orders'] as $value) { ?>
                                <tr id="<?php echo $value['OrderID']; ?>">
                                    <td>
                                        <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($value['UserID']); ?>"
                                           target="_blank"><?php echo $value['FullName']; ?></a></td>
                                    <td>
                                        <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($value['BoothID']); ?>"
                                           target="_blank"><?php echo $value['BoothName']; ?></a></td>
                                    <td><?php echo $value['OrderTrackID']; ?></td>
                                    <td><?php echo $value['OrderReceivedAt'] != '' ? getFormattedDateTime($value['OrderReceivedAt'], 'd-m-Y h:i:s A') : 'N/A'; ?></td>
                                    <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                        <td>
                                            <a href="<?php echo base_url('cms/order/view/' . $value['OrderID']); ?>"
                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                        class="material-icons"
                                                        title="Click to view details">visibility</i>
                                                <div class="ripple-container"></div>
                                            </a>
                                        </td>
                                    <?php } ?>
                                </tr>
                                <?php
                            }
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
                <div class="material-datatables tab-pane" id="completed_orders">
                    <table id="" class="datatable table table-striped table-no-bordered table-hover"
                           cellspacing="0" width="100%" style="width:100%">
                        <thead>
                        <tr>
                            <th>Customer Name</th>
                            <th>Booth Name</th>
                            <th>Order Tracking #</th>
                            <th>Order Received At</th>
                            <th><?php echo lang('actions'); ?></th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($orders['completed_orders']) {
                            foreach ($orders['completed_orders'] as $value) { ?>
                                <tr id="<?php echo $value['OrderID']; ?>">
                                    <td>
                                        <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($value['UserID']); ?>"
                                           target="_blank"><?php echo $value['FullName']; ?></a></td>
                                    <td>
                                        <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($value['BoothID']); ?>"
                                           target="_blank"><?php echo $value['BoothName']; ?></a></td>
                                    <td><?php echo $value['OrderTrackID']; ?></td>
                                    <td><?php echo $value['OrderReceivedAt'] != '' ? getFormattedDateTime($value['OrderReceivedAt'], 'd-m-Y h:i:s A') : 'N/A'; ?></td>
                                    <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                        <td>
                                            <a href="<?php echo base_url('cms/order/view/' . $value['OrderID']); ?>"
                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                        class="material-icons"
                                                        title="Click to view details">visibility</i>
                                                <div class="ripple-container"></div>
                                            </a>
                                        </td>
                                    <?php } ?>
                                </tr>
                                <?php
                            }
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
                <div class="material-datatables tab-pane" id="seller_cancelled">
                    <table id="" class="datatable table table-striped table-no-bordered table-hover"
                           cellspacing="0" width="100%" style="width:100%">
                        <thead>
                        <tr>
                            <th>Customer Name</th>
                            <th>Booth Name</th>
                            <th>Order Tracking #</th>
                            <th>Order Received At</th>
                            <th><?php echo lang('actions'); ?></th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($orders['cancelled_by_seller_orders']) {
                            foreach ($orders['cancelled_by_seller_orders'] as $value) { ?>
                                <tr id="<?php echo $value['OrderID']; ?>">
                                    <td>
                                        <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($value['UserID']); ?>"
                                           target="_blank"><?php echo $value['FullName']; ?></a></td>
                                    <td>
                                        <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($value['BoothID']); ?>"
                                           target="_blank"><?php echo $value['BoothName']; ?></a></td>
                                    <td><?php echo $value['OrderTrackID']; ?></td>
                                    <td><?php echo $value['OrderReceivedAt'] != '' ? getFormattedDateTime($value['OrderReceivedAt'], 'd-m-Y h:i:s A') : 'N/A'; ?></td>
                                    <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                        <td>
                                            <a href="<?php echo base_url('cms/order/view/' . $value['OrderID']); ?>"
                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                        class="material-icons"
                                                        title="Click to view details">visibility</i>
                                                <div class="ripple-container"></div>
                                            </a>
                                        </td>
                                    <?php } ?>
                                </tr>
                                <?php
                            }
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
                <div class="material-datatables tab-pane" id="buyer_cancelled">
                    <table id="" class="datatable table table-striped table-no-bordered table-hover"
                           cellspacing="0" width="100%" style="width:100%">
                        <thead>
                        <tr>
                            <th>Customer Name</th>
                            <th>Booth Name</th>
                            <th>Order Tracking #</th>
                            <th>Order Received At</th>
                            <th><?php echo lang('actions'); ?></th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($orders['cancelled_by_buyer_orders']) {
                            foreach ($orders['cancelled_by_buyer_orders'] as $value) { ?>
                                <tr id="<?php echo $value['OrderID']; ?>">
                                    <td>
                                        <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($value['UserID']); ?>"
                                           target="_blank"><?php echo $value['FullName']; ?></a></td>
                                    <td>
                                        <a href="<?php echo base_url('cms/user/view') . '/' . base64_encode($value['BoothID']); ?>"
                                           target="_blank"><?php echo $value['BoothName']; ?></a></td>
                                    <td><?php echo $value['OrderTrackID']; ?></td>
                                    <td><?php echo $value['OrderReceivedAt'] != '' ? getFormattedDateTime($value['OrderReceivedAt'], 'd-m-Y h:i:s A') : 'N/A'; ?></td>
                                    <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                        <td>
                                            <a href="<?php echo base_url('cms/order/view/' . $value['OrderID']); ?>"
                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                        class="material-icons"
                                                        title="Click to view details">visibility</i>
                                                <div class="ripple-container"></div>
                                            </a>
                                        </td>
                                    <?php } ?>
                                </tr>
                                <?php
                            }
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
            </div><!-- tab-content -->
        </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
</div>