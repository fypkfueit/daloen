

            
          
            <?php echo isset($message) ? $message : ''; ?>
            <!-- End order confirmed-->  
            <!--Order Summary-->      
           <!-- <tr><td height="25" colspan="4"><img style="height: auto; width: 560px;" src="<?php echo base_url('assets/header.png');?>"></td></tr>-->
            <tr><td height="15" colspan="4"></td></tr>
<?php if(!empty($requests)){
                        foreach ($requests as $request) {
                            $order_items = $request['items'];

                 ?>
                 <tr><td style="color: #000;font-weight:bold;padding-bottom: 0; padding-top: 0;" height="15" colspan="2"><b style="color:#FF9800;font-size: 16px;">Order Number:</b> <?php echo $request['OrderTrackID'];?></td></tr>
                 <tr><td style="color: #000;font-weight:bold;padding-bottom: 0; padding-top: 0;" height="15" colspan="2"><b style="color:#FF9800;font-size: 16px;">Seller:</b> <?php echo $request['BoothUserName'];?></td></tr>
            <tr style="font-size: 14px; font-weight: bold;">
                <td>Product</td>
                <td style="text-align: center;">Qty</td>
                <td style="text-align: center;">Price</td>
                <td style="text-align: center;">Total&nbsp;</td>
            </tr>
             <?php
                $total = 0;
                foreach($order_items as $key => $value){
                    $product_image = getProductImages($value['ProductID'],false);
                   $total= $total + ($value['PlaceOrderPrice']  * $value['Quantity']);?>
            <tr style="font-size:14px;">
                <td style="color: #000;font-weight:bold;padding-bottom:10px; padding-top:0;vertical-align: middle; line-height: 50px;"><img style="width: 50px; height: 50px; border-radius: 12px; margin-right: 15px; float: left;" src="<?php echo base_url($product_image);?>" width="50" height="50"><?php echo $value['Title'];?></td>
                <td style="color: #000;font-weight:bold;padding-bottom:10px; padding-top:0;vertical-align: middle;text-align: center;"><?php echo $value['Quantity']; ?></td>
                <td style="color: #000;font-weight:bold;padding-bottom:10px; padding-top:0;vertical-align: middle;text-align: center;"><?php echo $value['PlaceOrderPrice']; ?></td>
                <td style="color: #000;font-weight:bold;padding-bottom:10px; padding-top:0;vertical-align: middle;text-align: center;"><?php echo ($value['PlaceOrderPrice']  * $value['Quantity']); ?></td>
            </tr>

        <?php } ?>
            <tr><td style="padding: 0px 0;" height="15" colspan="4"></td></tr>
            <?php if($request['ActualDeliveryCharges'] > 0){ ?>
            <tr style="font-size: 14px;border-bottom:1pt solid #ddd;border-top:1pt solid #ddd;" border="0">
                <td style="border-bottom: 1pt solid #ddd;border-top: 1pt solid #ddd;"></td>
                <td colspan="2" style="border-bottom: 1pt solid #ddd;border-top: 1pt solid #ddd;text-align: right;color: #000;font-weight:bold;"><b style="color:#FF9800;font-size: 16px;">Delivery Charges:</b></td>
                <td style="border-bottom: 1pt solid #ddd;border-top: 1pt solid #ddd;color: #000;font-weight:bold;"><?php echo $request['ActualDeliveryCharges']; ?></td>
            </tr>
            <?php } ?> 
            <?php if($request['AdditionalDeliveryCharges'] > 0){ ?>
            <tr style="font-size: 14px;border-bottom:1pt solid #ddd;border-top:1pt solid #ddd;" border="0">
                <td style="border-bottom: 1pt solid #ddd;border-top: 1pt solid #ddd;"></td>
                <td colspan="2" style="border-bottom: 1pt solid #ddd;border-top: 1pt solid #ddd;text-align: right;color: #000;font-weight:bold;"><b style="color:#FF9800;font-size: 16px;">Additional Delivery Charges:</b></td>
                <td style="border-bottom: 1pt solid #ddd;border-top: 1pt solid #ddd;color: #000;font-weight:bold;"><?php echo $request['AdditionalDeliveryCharges']; ?></td>
            </tr> 
        <?php } ?>
        <?php if($request['VatAmountApplied'] > 0){ ?>
            <tr style="font-size: 14px;border-bottom:1pt solid #ddd;border-top:1pt solid #ddd;" border="0">
                <td style="border-bottom: 1pt solid #ddd;border-top: 1pt solid #ddd;"></td>
                <td colspan="2" style="border-bottom: 1pt solid #ddd;border-top: 1pt solid #ddd;text-align: right;color: #000;font-weight:bold;"><b style="color:#FF9800;font-size: 16px;">Vat Amount:</b></td>
                <td style="border-bottom: 1pt solid #ddd;border-top: 1pt solid #ddd;color: #000;font-weight:bold;"><?php echo $request['VatAmountApplied']; ?></td>
            </tr> 
        <?php } ?>
            
            <tr style="font-size: 14px;border-bottom:1pt solid #ddd;border-top:1pt solid #ddd;" border="0">
                <td colspan="3" style="border-bottom: 1pt solid #ddd;border-top: 1pt solid #ddd;text-align: right;color: #000;font-weight:bold;"><b style="color:#FF9800;font-size: 16px;">Grand Total:</b></td>
                <td style="border-bottom: 1pt solid #ddd;border-top: 1pt solid #ddd;color: #000;font-weight:bold;"><?php echo number_format(($total+$request['ActualDeliveryCharges']+$request['AdditionalDeliveryCharges']+$request['VatAmountApplied']),2,'.',''); ?></td>
            </tr>          
            
            
            <tr style="font-size: 14px;"><td height="15" colspan="4" style="padding: 0px 0;"></td></tr>
             <?php }  } ?>
             <?php if(isset($completed)){ ?>
            <tr><td  colspan="4" style="height:0.3px;font-size:14px;    text-align: center;"><p>For any approval inquiries, Please contact the seller(s)</p></td></tr>
            <?php } ?>

           <!-- <tr style="font-size: 14px;"><td height="15" colspan="4"></td></tr>

            <tr><td height="20" colspan="4" style="text-align: center;"> <img src="<?php // echo base_url('assets');?>/thank.jpg" width="150"/> </td></tr>

            <tr><td height="25" colspan="4" style="text-align: center;"><b style="color: #a264ad;">Thank you for your Order!</b></td></tr>

            <tr style="font-size: 14px;"><td height="15" colspan="4"></td></tr>-->

