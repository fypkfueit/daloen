<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Email_template extends Base_Controller {

    public $data = array();

    public function __construct() {

        parent::__construct();
        checkAdminSession();

        $this->load->Model([
            ucfirst($this->router->fetch_class()) . '_model',
            'Site_images_model'
        ]);




        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model'] = ucfirst($this->router->fetch_class()) . '_model';
        $this->data['TableKey'] = 'Email_templateID';
        $this->data['Table'] = 'email_templates';
    }

    public function index() {
        $parent = $this->data['Parent_model'];
       
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';

        $this->data['results'] = $this->$parent->getAll();

        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add() {
        if (!checkRightAccess(39, $this->session->userdata['admin']['RoleID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];


        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/add';



        $this->load->view('backend/layouts/default', $this->data);
    }

    public function edit($id = '') {
        if (!checkRightAccess(39, $this->session->userdata['admin']['RoleID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->get($id,false,'Email_templateID');
        $this->data['EmailFiles'] = $this->$parent->GetFilesOfEmail($id);

        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit';
        $this->data[$this->data['TableKey']] = $id;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function action() {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'update':
                $this->validate(1);
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;
        }
    }

    private function validate($update = false) {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($update) {
            if ($this->input->post('OldTitle') != $this->input->post('Title')) {
                $this->form_validation->set_rules('Title', lang('title'), 'required|is_unique[' . $this->data['Table'] . '.Title]');
            }
        } else {
            $this->form_validation->set_rules('Title', lang('title'), 'required|is_unique[' . $this->data['Table'] . '.Title]');
        }

        $this->form_validation->set_rules('Subject', 'Subject', 'required');
        $this->form_validation->set_rules('Description', lang('Description'), 'required');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function save() {

        if (!checkRightAccess(39, $this->session->userdata['admin']['RoleID'], 'CanAdd')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();

        $parent = $this->data['Parent_model'];
        
        $save_parent_data = array();
        $save_child_data = array();

        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);

        $sort = 0;
        if (!empty($getSortValue)) {

            $sort = $getSortValue['SortOrder'] + 1;
        }

        $save_parent_data['Title'] = $post_data['Title'];
        $save_parent_data['Subject'] = $post_data['Subject'];
        $save_parent_data['Description'] = $post_data['Description'];

       
        $save_parent_data['SortOrder'] = $sort;
        $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0 );
        $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
        $save_parent_data['CreatedBy'] = $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

        $insert_id = $this->$parent->save($save_parent_data);
        if ($insert_id > 0) {

            //Email Multiple Files
            if (isset($_FILES['EmailImage']) && !empty($_FILES['EmailImage'])) {
                $file = 'EmailImage';
                $path = 'uploads/email_templates/';
                $key = $insert_id;
                $type = 'EmailFile';
                $this->uploadImage($file, $path, $key, $type, TRUE);
            } else {
                $data['FileID'] = $insert_id;
                $data['ImageType'] = 'EmailFile';
                $data['ImageName'] = 'assets/backend/images/no_image.jpg';
                $this->Site_images_model->save($data);
            }
            //End

            // $default_lang = getDefaultLanguage();
            

            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($success);
            exit;
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function update() {

        if (!checkRightAccess(39, $this->session->userdata['admin']['RoleID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
//        print_rm($post_data);
        $parent = $this->data['Parent_model'];
       
        if (isset($post_data[$this->data['TableKey']])) {
            $id = base64_decode($post_data[$this->data['TableKey']]);
            $this->data['result'] = $this->$parent->get($id,false,'Email_templateID');

            if (!$this->data['result']) {
                $errors['error'] = lang('some_thing_went_wrong');
                $errors['success'] = false;
                $errors['redirect'] = true;
                $errors['url'] = 'cms/' . $this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }

            unset($post_data['form_type']);
            $save_parent_data = array();
            $save_child_data = array();
            

            $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0 );
            $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
            $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

            $save_parent_data['Title'] = $post_data['Title'];
            $save_parent_data['Subject'] = $post_data['Subject'];
            $save_parent_data['Description'] = $post_data['Description'];

           

            $update_by = array();
            $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);

            $this->$parent->update($save_parent_data, $update_by);

            //Email Multiple Files
            if (isset($_FILES['EmailImage'])) {
                $file = 'EmailImage';
                $path = 'uploads/email_templates/';
                $key = $id;
                $type = 'EmailFile';
                $this->uploadImage($file, $path, $key, $type, TRUE);
            }
            //End

            
            
            

            $success['error'] = false;
            $success['success'] = lang('update_successfully');

            echo json_encode($success);
            exit;
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
    }

    private function delete() {

        if (!checkRightAccess(39, $this->session->userdata['admin']['RoleID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }
        $parent = $this->data['Parent_model'];
       

        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        
        $this->$parent->delete($deleted_by);
        $EmailFiles = $this->$parent->GetFilesOfEmail($this->input->post('id'));
        if ($EmailFiles) {
            foreach ($EmailFiles as $file) {
                if (file_exists($file['ImageName'])) {
                    unlink($file['ImageName']);
                }
                $image_deleted_by['SiteImageID'] = $file['SiteImageID'];
                $this->Site_images_model->delete($image_deleted_by);
            }
        }

        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }

    public function ViewTemplate($id) {
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->get($id,false,'Email_templateID');

        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $this->data['EmailFiles'] = $this->$parent->GetFilesOfEmail($id);
        $this->data[$this->data['TableKey']] = $id;
        $this->load->view('backend/' . $this->data['ControllerName'] . '/EmailTemplate', $this->data);
    }

}
