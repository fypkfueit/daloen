<?php

/*
 * English language
 */

//General

$lang['some_thing_went_wrong'] = 'There is something went wrong';


$lang['save_successfully'] = 'تم الحفظ بنجاح!';
$lang['update_successfully'] = 'تم التحديث بنجاح!';
$lang['deleted_successfully'] = 'تم حذف المنتج بنجاح.';
$lang['you_should_update_data_seperately_for_each_language'] = 'يجب تحديث البينات لكل لغة';
$lang['yes'] = 'نعم';
$lang['no'] = 'لا';
$lang['actions'] = 'قرار';
$lang['submit'] = 'موافق / اشتري الآن / تسوق الآن / إنهاء العملية / تأكيد / تأكيد العملية / تسوق الحين';
$lang['back'] = 'العودة';
$lang['add'] = 'اضافة';
$lang['edit'] = 'تعديل';
$lang['are_you_sure'] = 'هل انت متأكد من الحذف؟';
$lang['view'] = 'عرض';
$lang['delete'] = 'Delete';
$lang['is_active'] = 'نشط';
$lang['create_table'] = 'انشاء جدول';
$lang['create_model'] = 'انشاء نموذج';
$lang['create_view'] = 'انشاء عرض';
$lang['create_controller'] = 'انشاء متحكم';
$lang['logout'] = 'تسجيل خروج';

$lang['please_add_role_first'] = 'الرجاء تحديد الصلاحيات اولا';
$lang['you_dont_have_its_access'] = 'لا توجد لديك صلاحية';

$lang['SiteName'] = 'اسم الموقع';
$lang['PhoneNumber'] = 'رقم الهاتف';
$lang['Whatsapp'] = 'واتساب';
$lang['Skype'] = 'سكايب';
$lang['Fax'] = 'فاكس';
$lang['SiteLogo'] = 'شعار الموقع';
$lang['EditSiteSettings'] = 'ضبط شعار الموقع';
$lang['FacebookUrl'] = 'رابط الفيسبوك';
$lang['GoogleUrl'] = 'رابط جوجل';
$lang['LinkedInUrl'] = 'رابط لينكد ان';
$lang['TwitterUrl'] = 'رابط تويتر';
//end General

// Module Section
$lang['choose_parent_module'] = 'اختيار النموذج الاساسي';
$lang['parent_module'] = 'النموذج الاساسي';
$lang['slug'] = 'Slug';
$lang['icon_class'] = 'ايقونة الفئة';

$lang['title'] = 'العنوان';
$lang['add_module'] = 'اضافة نموذج';
$lang['modules'] = 'النماذج';
$lang['module'] = 'النماذج';
$lang['module_rights'] = 'Module Rights';

$lang['please_add_module_first'] = 'الرجاء اضافة نموذج اولا';
$lang['rights'] = 'الحقوق';


// Roles Section

$lang['select_role'] = 'اختيار الصلاحية';
$lang['add_role'] = 'اضافة صلاحية';
$lang['roles'] = 'الصلاحيات';
$lang['role'] = 'صلاحية';
$lang['please_add_role_first'] = 'الرجاء تحديد الصلاحيات اولا';
$lang['choose_user_role'] = 'تحديد صلاحية المستخدم';


// activity section

$lang['add_activity'] = 'اضافة نشاط';
$lang['activities'] = 'نشاطات';
$lang['activity'] = 'نشاطات';
$lang['choose_activity'] = 'اختيار نشاط';

// user

$lang['please_add_user_first'] = 'الرجاء اختيار المستخدم اولا';

$lang['name'] = 'الاسم';
$lang['user'] = 'المستخدم';
$lang['users'] = 'المستخدمين';
$lang['add_user'] = 'اضافة مستخدم';
$lang['select_user'] = 'Select User';

$lang['email'] = 'البريد الالكتروني';
$lang['password'] = 'كلمة المرور';
$lang['min_length'] = '(Min char 8)';
$lang['confirm_password'] = 'تأكيد كلمة المرور';


$lang['add_city'] = 'اضافة مدينة';
$lang['citys'] = 'المدن';
$lang['city'] = 'المدينة';
$lang['add_district'] = 'اضافة حي';
$lang['districts'] = 'احياء';
$lang['district'] = 'حي';
$lang['please_add_distric_first'] = 'الرجاء اضافة حي اولا';
$lang['choose_district'] = 'اختيار الحي';
$lang['add_patient'] = 'Add Patient';
$lang['patients'] = 'Patients';
$lang['patient'] = 'Patient';
$lang['add_center'] = 'Add center';
$lang['centers'] = 'Centers';
$lang['center'] = 'Center';
$lang['add_screening_camp'] = 'Add Screening camp';
$lang['screening_camps'] = 'Screening camps';
$lang['screening_camp'] = 'Screening camp';
$lang['add_center_type'] = 'Add Center type';
$lang['center_types'] = 'Center Types';
$lang['center_type'] = 'Center Type';
$lang['add_email_template'] = 'اضافة قالب بريد الكتروني';
$lang['email_templates'] = 'قوالب بريد الكتروني';
$lang['email_template'] = 'قوالب بريد الكتروني';
$lang['Heading'] = 'Heading';
$lang['Description'] = 'الوصف';
$lang['Image'] = 'Image';
$lang['add_language'] = 'اضافة لغة';
$lang['languages'] = 'لغات';
$lang['language'] = 'لغات';
$lang['short_code'] = 'Short Code';

$lang['is_default'] = 'Is Default';
$lang['please_select_default_value_language_first'] = 'Please select another default value language first';
$lang['add_test'] = 'Add Test';
$lang['tests'] = 'Tests';
$lang['test'] = 'Test';
$lang['add_test'] = 'Add Test';
$lang['tests'] = 'Tests';
$lang['test'] = 'Test';
$lang['add_group'] = 'اضافة مجموعة';
$lang['groups'] = 'مجموعات';
$lang['group'] = 'Group';
$lang['add_feed'] = 'Add Feed';
$lang['feeds'] = 'Feeds';
$lang['feed'] = 'Feed';
$lang['add_category'] = 'اضافة فئة';
$lang['categorys'] = 'الفئات';
$lang['category'] = 'Category';
$lang['add_child_category'] = 'Add Child Category';
$lang['child_catagories_of'] = 'Child Catagories Of';
$lang['add_postReported'] = 'Add PostReported';
$lang['postReporteds'] = 'PostReporteds';
$lang['postReported'] = 'PostReported';
$lang['add_repotedComment'] = 'Add RepotedComment';
$lang['repotedComments'] = 'RepotedComments';
$lang['repotedComment'] = 'RepotedComment';
$lang['add_repotedGroup'] = 'Add RepotedGroup';
$lang['repotedGroups'] = 'RepotedGroups';
$lang['repotedGroup'] = 'RepotedGroup';
$lang['add_type'] = 'Add Type';
$lang['types'] = 'Types';
$lang['type'] = 'Type';
$lang['add_user_type'] = 'Add User_type';
$lang['user_types'] = 'User_types';
$lang['user_type'] = 'User_type';
//////////////////
$lang['something_went_wrong'] = 'حدث خطأ ما ، الرجاء المحاولة مرة اخرى';
$lang['registered_successfully'] = 'تم التسجيل بنجاح!';
$lang['username'] = 'اسم المستخدم';
$lang['mobile'] = 'رقم الجوال';
$lang['sorry_you_cant_login'] = 'عفوا لايمكنك تسجيل الدخول الان. الرجاء التواصل مع الدعم الفني';
$lang['logged_in_successfully'] = 'تم تسجيل الدخول بنجاح!';
$lang['user_not_found_with_these_login_details'] = 'لا يوجد اسم مستخدم مطابق للبيانات المدخلة';
$lang['user_not_found'] = 'لم يتم العثور على اسم المستخدم';
$lang['email_already_exist'] = 'البريد الالكتروني مستخدم بالفعل';
$lang['username_already_exist'] = 'اسم المستخدم مستخدم بالفعل';
$lang['booth_username_already_exist'] = 'اسم المستخدم للبوث مستخدم بالفعل';
$lang['mobile_already_exist'] = 'رقم الجوال مستخدم بالفعل';
$lang['user_logged_out_successfully'] = 'تم تسجيل الخروج بنجاح!';
$lang['password_reset_link_sent_in_email'] = 'تم ارسال رابط اعادة تعيين كلمة المرور لبريدك الالكتروني';
$lang['password_reset_link_sent_in_mobile'] = 'تم ارسال رابط اعادة تعيين كلمة المرور لرقم جوالك';
$lang['update_available_for_app'] = 'يوجد تحديث متاح للتطبيق!';
$lang['app_version_needed'] = 'نسخة التطبيق لم ترسل';
$lang['followed_successfully'] = 'تمت المتابعة بنجاح';
$lang['unfollowed_successfully'] = 'تم الغاء المتابعة بنجاح';
$lang['add_product'] = 'اضافة منتج';
$lang['products'] = 'منتجات';
$lang['product'] = 'منتجات';
$lang['please_verify_your_email'] = 'لم يتم تفعيل بريدك الالكتروني ، الرجاء تفعيله للمتابعة';
$lang['please_verify_your_mobile'] = 'لم يتم تفعيل رقم الجوال ، الرجاء تفعيله للمتابعة';
$lang['verification_code_sent'] = 'تم ارسال كود التحقق لرقم الجوال';
$lang['your_mobile_already_verified'] = 'رقم الجوال الخاص بك مفعل مسبقا';
$lang['mobile_no_verified'] = 'تم تفعيل رقم الجوال بنجاح!';
$lang['mobile_no_failed_to_verified'] = 'لقد فشلت عملية تفعيل رقم الجوال';
$lang['profile_updated'] = 'تم تحديث صورة ملفك الشخصي';
$lang['profile_updated_interest'] = 'تم التحديث بنجاح';
$lang['notification_marked_as_read'] = 'تم تحديد التنبيهات كمقروءة';
$lang['product_added_successfully'] = 'تم إضافة المنتج بنجاح.';
$lang['product_updated_successfully'] = 'تم تحديث المنتج بنجاح';
$lang['booth_name'] = 'اسم البوث';
$lang['product_type'] = 'نوع المنتج';
$lang['description'] = 'الوصف';
$lang['add_theme'] = 'اضافة تصميم';
$lang['themess'] = 'التصاميم';
$lang['themes'] = 'التصاميم';
$lang['theme'] = 'Theme';
$lang['user_already_reported'] = 'تم الابلاغ عن المستخدم مسبقا';
$lang['user_reported_successfully'] = 'تم الابلاغ عن المستخدم بنجاح';
$lang['user_already_blocked'] = 'تم حظر المستخدك مسبقا';
$lang['user_blocked_successfully'] = 'تم حظر المستخدم بنجاح';
$lang['user_unblocked_successfully'] = 'تم الغاء حظر المستخدم بنجاح';
$lang['profile_customized_successfully'] = 'تم تخصيص الملف الشخصي بنجاح';
$lang['feedback_sent_successfully'] = 'تم الإرسال بنجاح';
$lang['add_feedback'] = 'اضافة ملاحظات';
$lang['feedbacks'] = 'الملاحظات';
$lang['feedback'] = 'ملاحظة';
$lang['price'] = 'السعر';
$lang['currency'] = 'العملة';
$lang['out_of_stock'] = 'الكمية منتهية';
$lang['product_already_reported'] = 'تم الابلاغ عن المنتج مسبقا';
$lang['product_reported_successfully'] = 'تم الابلاغ عن المنتج بنجاح';
$lang['product_disliked_successfully'] = 'تم ازالة الاعجاب';
$lang['product_liked_successfully'] = 'تم اضافة اعجاب بالمنتج بنجاح';
$lang['product_already_liked'] = 'تم اضافة الاعجاب مسبقا';
$lang['comment_saved_successfully'] = 'تم حفظ التعليق بنجاح';
$lang['added_to_wishlist'] = 'تم إضافة المنتج إلى المفضلة';
$lang['removed_from_wishlist'] = 'تم ازالة المنتج من قائمة الامنيات بنجاح';
$lang['added_to_cart'] = 'تم إضافة المنتج إلى السلة';
$lang['cart_updated'] = 'تم تحديث سلة التسوق بنجاح';
$lang['product_not_available'] = 'عفوا هذا المنتج غير متاح!';
$lang['question_already_reported'] = 'تم الابلاغ عن الاستفسار مسبقا';
$lang['question_reported_successfully'] = 'تم الإبلاغ عن السؤال';
$lang['comment_notfound'] = 'Comment not found.';
$lang['comment_already_reported'] = 'Comment is already reported.';
$lang['comment_reported_successfully'] = 'Comment reported successfully.';
$lang['answear_comment_reported_successfully'] = 'Answear comment reported successfully.';
$lang['answear_comment_notfound'] = 'Answear comment not found.';
$lang['answear_comment_already_reported'] = 'Answear comment is already reported.';
$lang['your_cart_is_empty'] = 'سلة التسوق فارغة ، الرجاء اضافة منتجات الى السلة';
$lang['wrong_old_password'] = 'كلمة المرور القديمة خاطئة';
$lang['password_changed_successfully'] = 'تم تحديث كلمة المرور بنجاح. المرة استخدامها لتسجيل الدخول في المرة القادمة';
$lang['your_order_sent_for_approval'] = 'تم ارسال طلبك بنجاح! سيتم اشعارك عند تأكيد البوث لطلبك';
$lang['your_order_sent_for_approval_removed_products'] = 'شكرا لارسال طلبك.بعض المنتجات في طلبك غير متوفرة حاليا. و سيتم ابلاغك فورا حين يقوم البوث المعني بتأكيد طلبك';
$lang['order_approved'] = 'تمت الموافقة على الطلب بنجاح';
$lang['order_payment_done'] = 'تمت عملية الدفع بنجاح';
$lang['order_dispatched'] = 'تم إرسال الطلب بنجاح';
$lang['order_completed'] = 'تم إكمال الطلب بنجاح';
$lang['order_cancelled'] = 'تم الغاء الطلب بنجاح';
$lang['order_cant_be_cancelled'] = 'عفوا لا يمكنك الغاء الطلب الان';
$lang['order_status_cant_be_changed'] = 'لا يمك نغيير حالة الطلب';
$lang['order_status_cant_be_changed_wrong_code'] = 'يرجى إدخال رمز التوصيل لكي يتم إغلاق الطلب';
$lang['ticket_already_raised'] = 'تم تقديم طلب مساعدة على هذا الطلب مسبقا';
$lang['ticket_raised'] = 'تم تقديم طلب مساعدة بنجاح';
$lang['add_country'] = 'اضف دولة';
$lang['countrys'] = 'دول';
$lang['country'] = 'دولة';
$lang['chat_room_cant_be_deleted'] = 'لا يمكن مسح المحادثة لوجود رسائل لهذه المحادثة';
$lang['chat_room_deleted'] = 'تم حذف المحادثة بنجاح';
$lang['invitation_sent'] = 'تم ارسال الدعوة بنجاح';
$lang['invitation_already_sent_mobile'] = 'تم رسال دعوة مسبقة لهذا الرقم';
$lang['invitation_already_sent_email'] = 'تم رسال دعوة مسبقة لهذا البريد الالكتروني';
$lang['user_already_exist_with_mobile'] = 'يوجد مستخدم مسجل بالفعل بهذا الرقم';
$lang['user_already_exist_with_email'] = 'يوجد مستخدم مسجل بالفعل بهذا البريد الالكتروني';
$lang['coupon_applied'] = 'تم استخدام الكوبون بنجاح';
$lang['coupon_expired'] = 'الكوبون منتهي الصلاحية';
$lang['coupon_not_available_for_seller'] = 'هذا الكوبون غير متوفر مع هذا المستخدم';
$lang['coupon_invalid'] = 'الكوبون غير صحيح';
$lang['product_promoted'] = 'تم ارسال طلب الاعلان للقسم المعني ، سيتم الرد في اقرب وقت ممكن';
$lang['already_added_to_wishlist'] = 'تمت الإضافة مسبقاً إلى المفضلة';
$lang['product_already_promoted_for_store'] = 'لديك منتج مسبقا بانتظار اعتماد الاعلان';
$lang['promoted_product_not_found'] = 'لا يوجد منتج معلن عنه';
$lang['buyer_already_rated'] = 'عفوا لقد تم تقييم هذا المستخدم مسبقا';
$lang['seller_already_rated'] = 'عفوا لقد تم تقييم هذا البوث مسبقا';
$lang['buyer_rated'] = 'شكرا لقييمك لهذا المتسوق';
$lang['seller_rated'] = 'شكراًً لك على تقييمك لهذا البائع.';
$lang['sorry_cant_add_product_to_cart'] = 'عفوا لا يمكنك اضافة هذا المنتج لسلة التسوق';
$lang['product_already_rated'] = 'لقد تم تقييم هذا المنتج سابقا';
$lang['product_rated'] = 'شكراً لك على تقييمك لهذا المنتج.';
$lang['coupon_created_successfully'] = 'تم إنشاء كود الخصم بنجاح';
$lang['coupon_updated_successfully'] = 'تم تحديث كود الخصم بنجاح';
$lang['coupon_deleted_successfully'] = 'تم حذف كود الخصم بنجاح';
$lang['question_deleted_successfully'] = 'Question deleted successfully';
$lang['answear_deleted_successfully'] = 'Answear deleted successfully';
$lang['please_rate_booth_first'] = 'Please rate your last completed order to place a new order.';
$lang['coupon_must_contain_unique_value'] = 'يجب ان تحوي اكواد الخصم على رموز مميزة';
$lang['already_complained'] = 'لقد تم ارسال شكوى مسبقة على هذا الطلب';
$lang['promocode_limit'] = 'Yor promocode limit completed';
$lang['product_is_in_cart'] = 'You can\'t delete this prodcut.It is in order state';
$lang['same_password'] = 'Both old and new password are same.';
$lang['cannot_send_message'] = 'You can\'t send the message.';
$lang['you_cannot_do'] = 'You can\'t do this activity.';
$lang['already_login_some'] = 'You are already logged In on another device. In case you can\'t access to that device you can logged In by using forgot password.';

$lang['email_not_verify']            = 'Please verify your email first';
$lang['mobile_no_not_verify']            = 'Please verify your mobile no first';

$lang['post_added_successfully']            = 'Post added successfully';
$lang['post_reported_successfully'] = 'Post reported successfully.';
$lang['product_already_reported'] = 'Post is already reported.';
$lang['please_update_your_package'] = 'Account is no longer active.';
